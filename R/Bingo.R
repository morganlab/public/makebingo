# Functions for making a bingo board

make_grid <- function(d){
  my_grid <- tibble::tibble(
    xc=rep(c(1:d), times=d),
    yc=rep(c(1:d), each=d),
    size=5,
    fac = as.factor(yc),
    id = c(1:(d * d))
  )
}

#inputs: d = length of sides. m=MiddleBoardValue. f=terms
gen_board<- function(d, m, f) {
  mysam<-sample(f, (d * d -1))
  mysam<-stringr::str_wrap(mysam, width=10)
  foo<-c(mysam[1:round(d^2/2)], m, mysam[(round(d^2/2) + 1):(round(d^2) -1)])
  mygrid<-make_grid(d)
  mygrid$terms = foo
  return(mygrid)
}

plot_board<-function(d=5, m="Bingo", f) {
  my_grid<-gen_board(d, m, f)
  bingo<-ggplot2::ggplot(my_grid, ggplot2::aes(x=xc, y=yc, fill=fac)) +
    ggplot2::geom_point(shape=21, size=55)+
    ggplot2::geom_text(ggplot2::aes(label=terms), size=4.5) +
    ggplot2::theme(legend.position="none") +
    ggplot2::scale_x_continuous(limits=c(0, (d + 1))) +
    ggplot2::scale_y_continuous(limits=c(0, (d + 1))) +
    ggplot2::theme(axis.line=ggplot2::element_blank(), axis.text.x=ggplot2::element_blank(),
          axis.text.y=ggplot2::element_blank(),
          axis.ticks=ggplot2::element_blank(),
          axis.title.x=ggplot2::element_blank(),
          axis.title.y=ggplot2::element_blank(),
          legend.position="none",
          panel.background=ggplot2::element_blank(),
          panel.border=ggplot2::element_blank(),
          panel.grid.major=ggplot2::element_blank(),
          panel.grid.minor=ggplot2::element_blank(),
          plot.background=ggplot2::element_blank())
  return(bingo)
}
