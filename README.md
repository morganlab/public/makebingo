This is a small R package to randomly generate Bingo boards, using 
random subsets of terms you choose.

How to install:

devtools::install_gitlab("morganlab/public/makebingo")

See vignettes for examples of how to use. 

